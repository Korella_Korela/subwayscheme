from setuptools import setup, find_packages
from codecs import open
from os import path

def read(fname):
    return open(path.join(path.abspath(path.dirname(__file__)), fname)).read()

setup(
    name='subway_scheme',
    version='1.0',
    description='This allows you to create the Subway Scheme and find the optimal way',
    long_description=read('README'),
    author='Prokofyev Daniil',
    author_email='korela.dp@gmail.com',
    url='https://bitbucket.org/Korella_Korela/subway',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Win32 (MS Windows)',
        'Intended Audience :: End Users / Desktop',
        'Topic :: Utilities',
        'Natural Language :: English',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Scientific/Engineering :: Visualization',
    ],
    keywords='subway scheme',
    packages=find_packages(),
    install_requires=['pygame'],
)
