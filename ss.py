"""Create scheme and find the optimal path.

"""

import pickle #: pack and unpack the object data
from os import path #: find the script dir's abspath
import pygame as pg #: graphics

PG_INFO = None #: display info from pygame
NODESIZE = None #: screen based node size
SCREEN = None #: display size
CLOCK = None #: framerate ticks
FONT = None #: font style and size
COLORS = None #: list of available colors

class Graph(object):

    """Create, visualize and store the scheme.

    """

    def __init__(self):
        self.nodes = dict() #: [(x,y): node]
        self.way = list() #: [nodes]
        self.num = None #: way value (int)

    def addn(self, pos):
        """Add one node to the graph and set it's position
        Args:
            pos: cursor position (x, y)
        Returns:
            Node object
        """
        node = Node()
        node.setpos(pos, self)
        return node

    def link(self, event, node):
        """Link one node with another one
        Args:
            event(event): event data (from pygame module)
            node(Node): Node object for linking
        """
        one = None
        pos = posn(event)
        if not self.empty(pos):
            one = self.nodes[pos]
            if one is not node:
                if one not in node.adjacency:
                    node.adjacency.append(one)
                    one.adjacency.append(node)
                else:
                    node.adjacency.remove(one)
                    one.adjacency.remove(node)
                    if one.rect_area.topleft in node.value.keys():
                        node.value.pop(one)
                        one.value.pop(node)

    def deln(self, pos):
        """Del one node from the graph
        Args:
            pos(tuple): cursor position (x, y)
        """
        node = self.nodes.pop(pos)
        for adj in node.adjacency:
            adj.adjacency.remove(node)
            if adj.value.get(node.rect_area.topleft, None):
                adj.value.pop(node.rect_area.topleft)
        if node in self.way:
            self.way.clear()
            self.num = None
        del node

    def empty(self, pos):
        """Check area to be empty
        Args:
            pos(tuple): cursor position (x, y)
        Returns:
            True if area is empty, False otherwise
        """
        empty = True
        for area in self.nodes.values():
            if area.rect_area.collidepoint(pos):
                empty = False
        return empty

    def rofl(self, start, end):
        """Start the pathsfinding and choose the optimal one
        Args:
            start(Node): start point
            end(Node): goal point
        """
        paths = self.road(start, end)
        best_v = float('inf')
        best_p = []
        for way in paths:
            old = None
            cur_v = 0
            for node in way:
                try:
                    if old is not None:
                        cur_v += int(node.value.get(old.rect_area.topleft))
                except TypeError:
                    cur_v = float('inf')
                    break
                old = node
            if cur_v < best_v:
                best_v = cur_v
                best_p = way
        self.way.clear()
        self.way = best_p
        self.num = best_v

    def road(self, start, end, way=None):
        """Create list of lists which contains all available paths
        Args:
            start(Node): start point
            end(Node): goal point
            way(list): list of current path. Defaults to None.
        Returns:
            paths(list of list): contains all available paths
        """
        if way is None:
            way = list()
        way = way + [start]
        if start == end:
            return [way]
        paths = []
        for node in start.adjacency:
            if node not in way:
                newpaths = self.road(node, end, way)
                for newpath in newpaths:
                    paths.append(newpath)
        return paths

    def save(self):
        """Save the graph data

        """
        here = path.abspath(path.dirname(__file__))
        try:
            with open(path.join(here, 'graphinfo.swsm'), 'wb') as file:
                pickle.dump(self.nodes, file)
        except EOFError:
            pass

    def load(self):
        """Load the graph data

        """
        here = path.abspath(path.dirname(__file__))
        try:
            with open(path.join(here, 'graphinfo.swsm'), 'rb') as file:
                self.nodes = pickle.load(file)
        except EOFError:
            pass

    def update(self, selected):
        """Redraw the entire screen
        Args:
            selected(Node): selected Node object

        """
        screen_back_color = (0, 0, 0) #Black
        node_name_color = (255, 255, 255) #White
        node_path_color = (255, 0, 0) #Red
        SCREEN.fill(screen_back_color)
        for node in self.nodes.values():
            pg.draw.rect(SCREEN, node.color, node.rect_area)
            for adj in node.adjacency:
                if node.rect_area.left < adj.rect_area.left:
                    if node.rect_area.top < adj.rect_area.top:
                        pg.draw.line(SCREEN,
                                     node.color,
                                     node.rect_area.bottomright,
                                     adj.rect_area.topleft,
                                     1)
                    else:
                        pg.draw.line(SCREEN,
                                     node.color,
                                     node.rect_area.topright,
                                     adj.rect_area.bottomleft,
                                     1)

                else:
                    if node.rect_area.top < adj.rect_area.top:
                        pg.draw.line(SCREEN,
                                     node.color,
                                     node.rect_area.bottomleft,
                                     adj.rect_area.topright,
                                     1)
                    else:
                        pg.draw.line(SCREEN,
                                     node.color,
                                     node.rect_area.topleft,
                                     adj.rect_area.bottomright,
                                     1)
            if node.name:
                label = FONT.render(node.name,
                                    0,
                                    node_name_color)
                if node.rect_area.right+len(node.name)*10 < PG_INFO.current_w:
                    SCREEN.blit(label,
                                (node.rect_area.right+5,
                                 node.rect_area.top+4))
                else:
                    SCREEN.blit(label,
                                (node.rect_area.left-len(node.name)*10-5,
                                 node.rect_area.top+4))
            if node in self.way:
                pg.draw.rect(SCREEN, node_path_color, node.rect_area)

        if selected:
            for adj in selected.adjacency:
                if selected.value.get(adj.rect_area.topleft):
                    label = FONT.render(selected.value.get(adj.rect_area.topleft),
                                        0,
                                        node_name_color)
                    SCREEN.blit(label,
                                (adj.rect_area.left+11,
                                 adj.rect_area.top+7))

        if selected in self.nodes.values():
            pg.draw.rect(SCREEN, selected.color, selected.rect_area)

        if self.num and self.way and selected:
            label = FONT.render(str(self.num)+"m",
                                0,
                                node_name_color)
            SCREEN.blit(label,
                        (self.way[-1].rect_area.left+2,
                         self.way[-1].rect_area.top+7))


class Node(object):

    """Atom of the graph

    """

    def __init__(self):
        self.rect_area = None #: collision checking and drawing
        self.color = COLORS[0] #: color (r, g, b) for drawing
        self.color_number = 0 #: counter for checking the range of list. Max=14
        self.name = "" #: Maxlen=2.
        self.adjacency = list() #: list of neighbors. [nodes]
        self.value = dict() #: value=minutes [(x,y): int]

    def setpos(self, pos, graph):
        """Set node's position
        Args:
            pos(tuple): cursor position (x, y)
            graph(Graph): Graph object
        """
        if self.rect_area:
            graph.nodes.pop(self.rect_area.topleft, None)
        old_rect = self.rect_area
        self.rect_area = pg.Rect(pos[0], pos[1], NODESIZE, NODESIZE)
        graph.nodes[pos] = self
        for adj in self.adjacency:
            if adj.value.get(old_rect.topleft):
                old_rect_value = adj.value.pop(old_rect.topleft)
                adj.value[self.rect_area.topleft] = old_rect_value


def posn(event):
    """Create valid position
    Args:
        event(event): event data (from pygame module)
    Returns:
        pos(tuple): position
    """
    cur_x, cur_y = event.pos
    cur_x -= cur_x % NODESIZE
    cur_y -= cur_y % NODESIZE
    pos = (cur_x, cur_y)
    return pos

def init():
    """Start the program

    """
    global PG_INFO
    global NODESIZE
    global SCREEN
    global CLOCK
    global FONT
    global COLORS
    pg.init()
    pg.key.set_repeat(512, 16)
    PG_INFO = pg.display.Info()
    NODESIZE = round(PG_INFO.current_w/64)
    print("PG_INFO:\n", PG_INFO)
    SCREEN = pg.display.set_mode((PG_INFO.current_w, PG_INFO.current_h),
                                 pg.FULLSCREEN)
    CLOCK = pg.time.Clock()
    FONT = pg.font.SysFont("Lucida Console", round(PG_INFO.current_w/128))
    COLORS = list()
    COLORS.append((250, 128, 114)) #Blue
    COLORS.append((255, 182, 193)) #LightPink
    COLORS.append((255, 255, 0)) #Yellow
    COLORS.append((238, 130, 238)) #Violet
    COLORS.append((152, 251, 152)) #PaleGreen
    COLORS.append((50, 205, 50)) #LimeGreen
    COLORS.append((65, 105, 225)) #RoyalBlue
    COLORS.append((211, 211, 211)) #LightGray
    COLORS.append((0, 255, 255)) #Cyan
    COLORS.append((255, 165, 0)) #Orange
    COLORS.append((220, 20, 60)) #Crimson
    COLORS.append((240, 230, 140)) #Khaki
    COLORS.append((255, 69, 0)) #OrangeRed
    COLORS.append((240, 255, 255)) #Azure


def quit_all():
    """End the program

    """
    pg.quit()
    raise SystemExit()

def main():
    """The main logic

    """
    init()
    numbers = "123456789"
    alphabet = "qwertyuiopasdfghjklzxcvbnm"
    adkeys = ["space", "backspace", "delete"]
    framerate = 20
    max_color_count = 14
    max_name_len = 22
    graph = Graph()
    graph.load()
    selected = None
    old_node = None
    gucci = True

    while gucci:
        graph.update(selected)

        for event in pg.event.get():
            if event.type == pg.MOUSEBUTTONDOWN:
                pos = posn(event)
                if event.button == 1:
                    if graph.empty(pos):
                        selected = graph.addn(pos)
                    else:
                        if old_node != selected:
                            old_node = selected
                        selected = graph.nodes[pos]
                elif event.button == 2 and selected:
                    if not graph.empty(pos):
                        graph.link(event, selected)
                elif event.button == 3:
                    if not graph.empty(pos):
                        graph.deln(pos)
                elif event.button == 4 and selected:
                    if selected.color_number < max_color_count-1:
                        selected.color = COLORS[selected.color_number+1]
                        selected.color_number += 1
                    else:
                        selected.color = COLORS[0]
                        selected.color_number = 0
                elif event.button == 5 and selected:
                    if selected.color_number > 0:
                        selected.color = COLORS[selected.color_number-1]
                        selected.color_number -= 1
                    else:
                        selected.color = COLORS[max_color_count-1]
                        selected.color_number = max_color_count-1

            elif event.type == pg.MOUSEMOTION:
                if event.buttons[0] and selected:
                    pos = posn(event)
                    if graph.empty(pos):
                        selected.setpos(pos, graph)

            elif event.type == pg.KEYDOWN and selected:
                if len(selected.name) <= max_name_len:
                    if ((pg.key.name(event.key) in alphabet) and
                            (pg.key.name(event.key) not in adkeys)):
                        selected.name += pg.key.name(event.key).upper()
                if pg.key.name(event.key) == "space":
                    selected.name += " "
                if pg.key.name(event.key) == "backspace":
                    selected.name = selected.name[:-1]

                if old_node in selected.adjacency:
                    if ((pg.key.name(event.key) in numbers) and
                            (pg.key.name(event.key) not in adkeys)):
                        old_node.value[selected.rect_area.topleft] = pg.key.name(event.key)
                        selected.value[old_node.rect_area.topleft] = pg.key.name(event.key)
                    if pg.key.name(event.key) == "delete":
                        old_node.value[selected.rect_area.topleft] = ' '
                        selected.value[old_node.rect_area.topleft] = ' '

                if ((pg.key.name(event.key) == "home") and
                        (old_node) and
                        (old_node is not selected)):
                    graph.rofl(old_node, selected)

                if pg.key.name(event.key) == "escape":
                    gucci = False

        pg.display.update()
        CLOCK.tick(framerate)

    graph.save()
    quit_all()

if __name__ == '__main__':
    main()
